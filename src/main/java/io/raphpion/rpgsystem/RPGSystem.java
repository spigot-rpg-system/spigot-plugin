package io.raphpion.rpgsystem;

import io.raphpion.rpgsystem.controllers.CommandManager;
import io.raphpion.rpgsystem.controllers.ListenerManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class RPGSystem extends JavaPlugin {

    @Override
    public void onEnable() {
        getConfig().options().copyDefaults();
        saveDefaultConfig();

        ListenerManager.registerEvents(this);
        CommandManager.registerCommands(this);

        getLogger().info("Plugin successfully enabled.");
    }

}
