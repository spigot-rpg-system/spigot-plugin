package io.raphpion.rpgsystem.infrastructures.repositories;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

import io.raphpion.rpgsystem.RPGSystem;

public abstract class RepositoryBase {

    protected static final MongoClient client = MongoClients.create(RPGSystem.getPlugin(RPGSystem.class).getConfig().getString("db_connection_string"));

}
