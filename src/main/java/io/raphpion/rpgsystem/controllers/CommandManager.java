package io.raphpion.rpgsystem.controllers;

import io.raphpion.rpgsystem.controllers.commands.CharacterCommands;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class CommandManager {

    public static void registerCommands(JavaPlugin p_javaPlugin) {
        // register character commands
        CharacterCommands characterCommands = new CharacterCommands();
        p_javaPlugin.getCommand("character").setExecutor(characterCommands);
    }

    public static void sendNoPermissionMessage(Player p_player) {
        p_player.sendMessage("§4You don't have permission to use this command!");
    }

}
