package io.raphpion.rpgsystem.controllers.commands;

import io.raphpion.rpgsystem.controllers.CommandManager;
import io.raphpion.rpgsystem.entities.Character;
import io.raphpion.rpgsystem.infrastructures.repositories.CharacterRepository;
import io.raphpion.rpgsystem.services.CharacterService;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CharacterCommands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("character")) {
            /*
             * /character level ========================================================================================
             */
            if (args[0].equalsIgnoreCase("level")) {
                switch(args.length) {
                    /*
                     * /character level set <player> <integer>
                     */
                    case 4:
                        if (args[1].equalsIgnoreCase("set")) {
                            Player target = Bukkit.getPlayer(args[2]);
                            if (target != null) {
                                int level = Integer.parseInt(args[3]);
                                if (level > 0 && level <= 100) {
                                    if (player.hasPermission("rpg.character.level.set.others")) {
                                        Character character = CharacterService.register(target.getUniqueId().toString());
                                        character.setLevel(level);
                                        new CharacterRepository().save(character);
                                        player.sendMessage("§2Set " + args[2] + "'s level to " + level + ".");
                                    } else {
                                        CommandManager.sendNoPermissionMessage(player);
                                    }
                                } else {
                                    player.sendMessage("§4You need to enter an integer between 1 and 100!");
                                }
                            } else {
                                player.sendMessage("§4Player " + args[2] + " could not be found!");
                            }
                        } else {
                            player.sendMessage("§4Invalid usage. Try /character level set <player> <integer> instead.");
                        }
                        break;
                    /*
                     * /character level set <integer>
                     */
                    case 3:
                        if (args[1].equalsIgnoreCase("set")) {
                            int level = Integer.parseInt(args[2]);
                            if (level > 0 && level <= 100) {
                                if (player.hasPermission("rpg.character.level.set")) {
                                    Character character = CharacterService.register(player.getUniqueId().toString());
                                    character.setLevel(level);
                                    new CharacterRepository().save(character);
                                    player.sendMessage("§2Set level to " + level + ".");
                                } else {
                                    CommandManager.sendNoPermissionMessage(player);
                                }
                            } else {
                                player.sendMessage("§4You need to enter an integer between 1 and 100!");
                            }
                        } else {
                            player.sendMessage("§4Invalid usage. Try /character level set <integer> instead.");
                        }
                        break;
                    /*
                     * /character level <player>
                     */
                    case 2:
                        Player target = Bukkit.getPlayer(args[1]);
                        if (target != null) {
                            if (player.hasPermission("rpg.character.level.others")) {
                                player.sendMessage("§2" + args[1] + "'s level is " + CharacterService.register(target.getUniqueId().toString()).getLevel() + ".");
                            } else {
                                CommandManager.sendNoPermissionMessage(player);
                            }
                        } else {
                            player.sendMessage("§4Player " + args[1] + " could not be found!");
                        }
                        break;
                    /*
                     * /character level
                     */
                    case 1:
                        if (player.hasPermission("rpg.character.level")) {
                            player.sendMessage("§2Level: " + CharacterService.register(player.getUniqueId().toString()).getLevel());
                        } else {
                            CommandManager.sendNoPermissionMessage(player);
                        }
                        break;
                    default:
                        player.sendMessage("§4Invalid usage. Try /character level [set?] <int> instead.");
                }
                return true;
            }
            /*
             * /character exp ==========================================================================================
             */
            else if (args[0].equalsIgnoreCase("exp")) {
                switch(args.length) {
                    /*
                     * /character exp <set/add/remove> <player> <integer>
                     */
                    case 4:
                        Player target = Bukkit.getPlayer(args[2]);
                        if (target != null) {
                            int experience = Integer.parseInt(args[3]);
                            if (experience >= 0) {
                                if (player.hasPermission("rpg.character.exp.modify.others")) {
                                    Character character = CharacterService.register(target.getUniqueId().toString());
                                    switch (args[1]) {
                                        case "set":
                                            character.setExperience(experience);
                                            new CharacterRepository().save(character);
                                            player.sendMessage("§2Set " + args[2] + "'s experience to " + experience + ".");
                                            break;
                                        case "add":
                                            character.addExperience(experience);
                                            new CharacterRepository().save(character);
                                            player.sendMessage("§2Gave " + args[2] + " " + experience + " experience points.");
                                            break;
                                        case "remove":
                                            character.addExperience(-experience);
                                            new CharacterRepository().save(character);
                                            player.sendMessage("§2Removed " + args[2] + " " + experience + " experience points.");
                                            break;
                                        default:
                                            player.sendMessage("§4Invalid usage! Try /character exp <set/add/remove> <Player> <integer> instead.");
                                    }
                                } else {
                                    CommandManager.sendNoPermissionMessage(player);
                                }
                            } else {
                                player.sendMessage("§4You need to enter an unsigned integer!");
                            }
                        } else {
                            player.sendMessage("§4Player " + args[2] + " could not be found!");
                        }
                        break;
                    /*
                     * /character exp <set/add/remove> <integer>
                     */
                    case 3:
                        int experience = Integer.parseInt(args[2]);
                        if (experience >= 0) {
                            if (player.hasPermission("rpg.character.exp.modify")) {
                                Character character = CharacterService.register(player.getUniqueId().toString());
                                switch(args[1]) {
                                    case "set":
                                        character.setExperience(experience);
                                        new CharacterRepository().save(character);
                                        player.sendMessage("§2Set experience to " + experience + ".");
                                        break;
                                    case "add":
                                        character.addExperience(experience);
                                        new CharacterRepository().save(character);
                                        player.sendMessage("§2Gained " + experience + " experience.");
                                        break;
                                    case "remove":
                                        character.addExperience(-experience);
                                        new CharacterRepository().save(character);
                                        player.sendMessage("§2Removed " + experience + " experience.");
                                        break;
                                    default:
                                        player.sendMessage("§4Invalid usage. Try /character exp <set/add/remove> <integer> instead.");
                                }
                            } else {
                                CommandManager.sendNoPermissionMessage(player);
                            }
                        } else {
                            player.sendMessage("§4You need to enter an unsigned integer.");
                        }
                        break;
                    /*
                     * /character exp <player>
                     */
                    case 2:
                        target = Bukkit.getPlayer(args[1]);
                        if (target != null) {
                            if (player.hasPermission("rpg.character.exp.others")) {
                                player.sendMessage("§2" + args[1] + "'s experience is " + CharacterService.register(target.getUniqueId().toString()).getExperience() + ".");
                            } else {
                                CommandManager.sendNoPermissionMessage(player);
                            }
                        } else {
                            player.sendMessage("§4Player " + args[1] + " could not be found!");
                        }
                        break;
                    /*
                     * /character exp
                     */
                    case 1:
                        if (player.hasPermission("rpg.character.exp")) {
                            player.sendMessage("§2Experience: " + CharacterService.register(player.getUniqueId().toString()).getExperience());
                        } else {
                            CommandManager.sendNoPermissionMessage(player);
                        }
                        break;
                    default:
                        player.sendMessage("§4Invalid usage.");
                }
                return true;
            }
        }
        return true;
    }
}
