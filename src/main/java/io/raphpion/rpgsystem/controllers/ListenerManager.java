package io.raphpion.rpgsystem.controllers;

import io.raphpion.rpgsystem.controllers.listeners.PlayerJoinListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class ListenerManager {

    public static void registerEvents(JavaPlugin p_javaPlugin) {
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoinListener(), p_javaPlugin);
    }

}
